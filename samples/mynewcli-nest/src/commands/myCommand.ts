import { BaseCommand } from 'nestjs-console-oclif';
import { flags } from '@oclif/command';

export class MyCommand extends BaseCommand {
  static description = 'description of this example command';

  static flags = {
    myFlag: flags.string({
      char: 'f',
      description: 'My flag',
    }),
  };

  static args = [{ name: 'myArg' }];
}
