#!/usr/bin/env node

import { bootstrap } from 'nestjs-console-oclif';
import { AppModule } from './app.module';
import { MyCommand } from './commands/myCommand';

bootstrap({
  nestModule: AppModule,
  singleCommand: MyCommand,
});
