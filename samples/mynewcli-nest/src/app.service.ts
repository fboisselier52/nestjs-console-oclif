import { Injectable } from '@nestjs/common';
import { OclifCommand } from 'nestjs-console-oclif';

@Injectable()
export class AppService {
  @OclifCommand('myCommand')
  public getHello(): void {
    console.log('Hello World!');
  }
}
