#!/usr/bin/env node

import { bootstrap } from 'nestjs-console-oclif';
import { AppModule } from './app.module';

bootstrap({
  nestModule: AppModule,
});
