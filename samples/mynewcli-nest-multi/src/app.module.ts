import { Module } from '@nestjs/common';
import { OclifModule } from 'nestjs-console-oclif';
import { AppService } from './app.service';

@Module({
  imports: [OclifModule.forRoot()],
  providers: [AppService],
})
export class AppModule {}
