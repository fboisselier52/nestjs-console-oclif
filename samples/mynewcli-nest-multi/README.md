Main project : [nestjs-console-oclif](https://gitlab.com/fboisselier52/nestjs-console-oclif)

## Description

This project is a sample for a multi command.

## Installation

```bash
$ npm install
```

## Running the app

```bash
# Build
$ npm run build

# start
$ ./dist/main.js

# start globally
$ npm install -g
$ myNewCliNest
```
