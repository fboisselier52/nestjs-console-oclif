import { Inject } from '@nestjs/common';

export const flagKeys: string[] = [];

export function OclifFlags(key: string = '') {
  if (!flagKeys.includes(key)) {
    flagKeys.push(key);
  }
  return Inject(key === '' ? 'oclif.flags' : `oclif.flags.${key}`);
}
