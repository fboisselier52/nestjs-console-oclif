import { DynamicModule, Provider, Module } from '@nestjs/common';
import { oclifProviders } from './oclif.providers';
import { OclifService } from './oclif.service';
import { OclifModuleOptions } from './oclif.module.options';

@Module({
  providers: [OclifService],
  exports: [OclifService],
})
export class OclifModule {
  static forRoot(options: OclifModuleOptions = {}): DynamicModule {
    const providers: Provider[] = oclifProviders(options.provideFlags, options.provideArgs);
    return {
      module: OclifModule,
      providers,
      exports: providers,
    };
  }
}
