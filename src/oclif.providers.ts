import { Provider } from '@nestjs/common';
import { argKeys } from './oclifArgs.decorator';
import { flagKeys } from './oclifFlags.decorator';
import { BaseCommand } from './base.command';

function buildProviderFor(type: 'args' | 'flags'): (key: string) => Provider {
  return (key: string): Provider => {
    if (key === '') {
      return {
        provide: `oclif.${type}`,
        useFactory: () => BaseCommand.cmdContext[type],
      };
    }
    return {
      provide: `oclif.${type}.${key}`,
      useFactory: () => BaseCommand.cmdContext[type][key],
    };
  };
}

export function oclifProviders(provideFlags?: string[], provideArgs?: string[]): Provider[] {
  const flags = new Set(flagKeys.concat(provideFlags || []));
  const flagsProviders: Provider[] = Array.from(flags).map(buildProviderFor('flags'));

  const args = new Set(argKeys.concat(provideArgs || []));
  const argsProviders: Provider[] = Array.from(args).map(buildProviderFor('args'));

  return flagsProviders.concat(argsProviders);
}
