export interface Entrypoint {
  commandClass: string;
  serviceClass: any;
  serviceMethod: string;
}

export const entrypoints: Entrypoint[] = [];

export function OclifCommand(commandClass: string) {
  return (serviceClass: any, serviceMethod: string) => {
    if (entrypoints.map((e: Entrypoint) => e.commandClass).includes(commandClass)) {
      throw new Error(`Multiple OclifCommand declarations for command ${commandClass}`);
    }
    entrypoints.push({ commandClass, serviceClass, serviceMethod });
  };
}
