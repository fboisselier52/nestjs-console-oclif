import { INestApplicationContext, Injectable } from '@nestjs/common';
import { entrypoints, Entrypoint } from './oclifCommand.decorator';

@Injectable()
export class OclifService {
  async run(app: INestApplicationContext, commandClass: string) {
    const filtered = entrypoints.filter(
      (e: Entrypoint) => e.commandClass.toLocaleLowerCase() === commandClass.toLocaleLowerCase(),
    );
    if (filtered.length === 0) {
      throw new Error(`Cannot find entrypoint for ${commandClass}`);
    }
    const entrypoint: Entrypoint = filtered[0];
    await app.get(entrypoint.serviceClass.constructor)[entrypoint.serviceMethod]();
  }
}
