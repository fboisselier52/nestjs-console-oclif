import Command from '@oclif/command';
import { Input as InputF, help } from '@oclif/command/lib/flags';
import { Input as InputP } from '@oclif/parser';
import { INestApplicationContext } from '@nestjs/common';
import { NestFactory } from '@nestjs/core';
import { CmdContext } from './cmd.dtos';
import { OclifService } from './oclif.service';
import { BootstrapOptions } from './bootstrap.options';

export abstract class BaseCommand extends Command {
  static flags: InputF<any> = {
    help: help({ char: 'h' }),
  };

  protected app: INestApplicationContext;

  static cmdContext: CmdContext;

  private static bootstrapOptions: BootstrapOptions;

  static register(bootstrapOptions: BootstrapOptions) {
    BaseCommand.bootstrapOptions = bootstrapOptions;
  }

  async init() {
    const output = this.parse(this.constructor as InputP<any>);

    BaseCommand.cmdContext = {
      args: output.args,
      flags: output.flags,
    };

    this.app = await NestFactory.createApplicationContext(BaseCommand.bootstrapOptions.nestModule);
    if (BaseCommand.bootstrapOptions.enableShutdownHooks) {
      this.app.enableShutdownHooks();
    }
  }

  async run(): Promise<void> {
    await this.app.get(OclifService).run(this.app, this.constructor.name);
  }

  async finally() {
    if (this.app) {
      await this.app.close();
    }
  }
}
