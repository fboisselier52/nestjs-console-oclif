import { OutputArgs, OutputFlags } from '@oclif/parser';

export interface CmdContext {
  flags: OutputFlags<any>;
  args: OutputArgs<any>;
}
