import { handle } from '@oclif/errors/lib/handle';
import { run } from '@oclif/command';
import { BaseCommand } from './base.command';
import { BootstrapOptions } from './bootstrap.options';

export async function bootstrap(options: BootstrapOptions) {
  try {
    BaseCommand.register(options);
    if (options.singleCommand) {
      await options.singleCommand.run();
    } else {
      await run(null, module.parent && module.parent.parent && module.parent.parent.filename || __dirname);
    }
  } catch (error) {
    handle(error);
  }
}
