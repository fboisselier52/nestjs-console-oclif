import {BaseCommand} from './base.command';

export class BootstrapOptions {
  nestModule: any;

  singleCommand?: typeof BaseCommand;

  enableShutdownHooks?: boolean;
}
