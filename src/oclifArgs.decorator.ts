import { Inject } from '@nestjs/common';

export const argKeys: string[] = [];

export function OclifArgs(key: string = '') {
  if (!argKeys.includes(key)) {
    argKeys.push(key);
  }
  return Inject(key === '' ? 'oclif.args' : `oclif.args.${key}`);
}
