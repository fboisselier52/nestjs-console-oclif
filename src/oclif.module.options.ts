export interface OclifModuleOptions {
  provideFlags?: string[];
  provideArgs?: string[];
}
